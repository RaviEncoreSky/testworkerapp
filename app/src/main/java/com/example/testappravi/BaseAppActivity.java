package com.example.testappravi;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import com.example.testappravi.databinding.ActivityBaseAppBinding;

public class BaseAppActivity extends AppCompatActivity {
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityBaseAppBinding activityBaseAppBinding  = ActivityBaseAppBinding.inflate(getLayoutInflater());
        setContentView(activityBaseAppBinding.getRoot());
    }

    void displayProgressDialog() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading..."); // Setting Message
        pDialog.setTitle("Progress Dialog"); // Setting Title
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        pDialog.show(); // Display Progress Dialog
        pDialog.setCancelable(false);
    }

}