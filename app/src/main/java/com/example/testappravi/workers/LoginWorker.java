package com.example.testappravi.workers;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;

import com.example.testappravi.eventsHandlers.LoginEvent;
import com.example.testappravi.networkHelper.ApiTestClass;
import com.example.testappravi.responseBody.LoginResponse;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Response;

public class LoginWorker extends BaseWorker<LoginResponse> {

    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    String email, password;

    public LoginWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        email= workerParams.getInputData().getString(EMAIL);
        password = workerParams.getInputData().getString(PASSWORD);

    }

    @NonNull
    @Override
    protected Call call() {
        return ApiTestClass.getClient().login(email,password);
    }

    @Override
    protected void onSuccess(LoginResponse loginResponse) {

        publishResult("Login Sucess \n"+loginResponse.getToken(),true);
        System.out.println("---success " + loginResponse.getToken());

    }

    @Override
    protected void onFailure(@NonNull Response response) {
        System.out.println("---fail " +response.errorBody().toString());
        publishResult("Login Failed \n"+response.errorBody().toString(),false);
    }

    @Override
    protected void onCancel() {
        System.out.println("---cancled ");
        publishResult(" onCanceled called",false);

    }

    private void publishResult(String message,Boolean success){

        EventBus.getDefault().post(new LoginEvent(message,success));
    }
}
