package com.example.testappravi.workers;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;

import com.example.testappravi.eventsHandlers.MovieListEvent;
import com.example.testappravi.networkHelper.MoviesApiClass;
import com.example.testappravi.responseBody.MovieResponse;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class MovieListWorker extends BaseWorker<List<MovieResponse>>{

    public MovieListWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    protected Call<List<MovieResponse>> call() {
        return MoviesApiClass.getClient().getMoviesList();
    }

    @Override
    protected void onSuccess(List<MovieResponse> movieResponse) {
        String message = movieResponse.toString();
        List<MovieResponse> movieResponseList = movieResponse;
        publishResult(message,movieResponseList);
        System.out.println("---success 1"+ message );
    }

    @Override
    protected void onFailure(@NonNull Response<List<MovieResponse>> response) {
        String message1 = response.errorBody().toString();
        publishResult(message1,null);
        System.out.println("---failure " + message1);

    }

    @Override
    protected void onCancel() {
        System.out.println("---canceled ");
        publishResult("On Cancel Called",null);
    }

    private void publishResult(String message, List<MovieResponse> movieResponseList){

        EventBus.getDefault().post(new MovieListEvent(message,movieResponseList));
    }



}
