package com.example.testappravi.workers;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;
import com.example.testappravi.eventsHandlers.RegisterEvent;
import com.example.testappravi.networkHelper.ApiTestClass;
import com.example.testappravi.responseBody.RegisterResponse;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Response;

public class RegisterWorker extends BaseWorker<RegisterResponse> {

    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    String email, password;

    public RegisterWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        email= workerParams.getInputData().getString(EMAIL);
        password = workerParams.getInputData().getString(PASSWORD);
    }

    @NonNull
    @Override
    protected Call call() {
        return ApiTestClass.getClient().registration(email,password);
    }

    @Override
    protected void onSuccess(RegisterResponse registerResponse) {
        System.out.println("---success " + registerResponse.getId());
        publishResult("success User Id  " + registerResponse.getId(),true);

    }

    @Override
    protected void onFailure(@NonNull Response response) {

        String msg = "";
        try {
            JSONObject jsonObject = new JSONObject(response.errorBody().string());
            msg = jsonObject.getString("error");
            System.out.println("Error " + msg);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("excep " + e.toString());
        }

        publishResult("Register Fail \n"+ response.errorBody().toString(),false);

    }

    @Override
    protected void onCancel() {
        System.out.println("---canceled ");
        publishResult("onCancel Called",false);

    }

    private void publishResult(String message,Boolean success){

        EventBus.getDefault().post(new RegisterEvent(message,success));
    }
}
