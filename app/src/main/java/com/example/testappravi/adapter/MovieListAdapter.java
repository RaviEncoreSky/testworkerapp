package com.example.testappravi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.testappravi.databinding.MovieItemListBinding;
import com.example.testappravi.responseBody.MovieResponse;

import java.util.List;

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.MyViewHolder> {
    List<MovieResponse> moviesListResponseData;
    Context mContext;

    public MovieListAdapter(List<MovieResponse> moviesListResponseData, Context mContext) {
        this.moviesListResponseData = moviesListResponseData;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MovieListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

      return new MyViewHolder(MovieItemListBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.tvTitle.setText(moviesListResponseData.get(position).getTitle());
        holder.tvRating.setText("Rating : "+moviesListResponseData.get(position).getRating()+" / 10");
        holder.tvYear.setText("Year : "+moviesListResponseData.get(position).getReleaseYear());
        Glide.with(mContext).load(moviesListResponseData.get(position).getImage())
                .into(holder.ivMovieImage);
    }

    @Override
    public int getItemCount() {
        return moviesListResponseData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private TextView tvTitle, tvYear, tvRating;
        private ImageView ivMovieImage;

        MyViewHolder(MovieItemListBinding itemBinding) {
            super(itemBinding.getRoot());
            ivMovieImage = itemBinding.ivMovieImage ;
            tvTitle = itemBinding.tvTitle ;
            tvYear = itemBinding.tvYear ;
            tvRating = itemBinding.tvRating ;
        }

    }

}