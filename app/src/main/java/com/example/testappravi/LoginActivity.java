package com.example.testappravi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;

import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.example.testappravi.databinding.ActivityLoginBinding;
import com.example.testappravi.eventsHandlers.LoginEvent;
import com.example.testappravi.workers.LoginWorker;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Objects;

public class LoginActivity extends BaseAppActivity {

    SharedPreferences Shared_pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityLoginBinding activityLoginBinding  = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(activityLoginBinding.getRoot());

        Shared_pref = getSharedPreferences("details", MODE_PRIVATE);
        Intent intent = new Intent(LoginActivity.this, UserHomeActivty.class);
//        Checking User Already Logged or Not
        if (Shared_pref.contains("user")) {
            startActivity(intent);
            finish();
        }

        activityLoginBinding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = activityLoginBinding.etEmail.getText().toString();
                String password = activityLoginBinding.etPassword.getText().toString();

                if (email.isEmpty() || password.isEmpty()) {

                    Toast.makeText(LoginActivity.this, "Please fill all details ", Toast.LENGTH_SHORT).show();

                }
                else {
                    Data workerData = new Data.Builder()
                            .putString(LoginWorker.EMAIL, (Objects.requireNonNull(email)))
                            .putString(LoginWorker.PASSWORD, (Objects.requireNonNull(password)))
                            .build();

                    WorkManager.getInstance().enqueue(new OneTimeWorkRequest
                            .Builder(LoginWorker.class)
                            .setInputData(workerData).build());

                    activityLoginBinding.etEmail.setText("");
                    activityLoginBinding.etPassword.setText("");

                    displayProgressDialog();
                }

            }
        });

        activityLoginBinding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

//    This method will be called when a LoginEvent is posted
    @Subscribe
    public void onEvent(LoginEvent loginEvent) {

        pDialog.dismiss();
        Looper.prepare();
        Toast.makeText(this, loginEvent.message+"  " + loginEvent.success, Toast.LENGTH_SHORT).show();

        if(loginEvent.success){
            Intent intent = new Intent(LoginActivity.this,UserHomeActivty.class);

            saveUser("logged"); // Save in Shared Preference
            startActivity(intent);
            finish();

        }

    }

    private void saveUser(String email) {
        SharedPreferences.Editor editor = Shared_pref.edit();
        editor.putString("user",email);
        editor.apply();
    }

}