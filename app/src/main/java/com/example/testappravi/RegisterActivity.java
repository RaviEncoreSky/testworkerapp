package com.example.testappravi;

import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;

import com.example.testappravi.databinding.ActivityRegisterBinding;
import com.example.testappravi.eventsHandlers.RegisterEvent;
import com.example.testappravi.workers.RegisterWorker;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Objects;

public class RegisterActivity extends BaseAppActivity {
    SharedPreferences Shared_pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityRegisterBinding activityRegisterBinding = ActivityRegisterBinding.inflate(getLayoutInflater());
        setContentView(activityRegisterBinding.getRoot());

        Shared_pref = getSharedPreferences("details", MODE_PRIVATE);

        activityRegisterBinding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = activityRegisterBinding.etEmail.getText().toString();
                String password = activityRegisterBinding.etPassword.getText().toString();

                if (email.isEmpty() || password.isEmpty()) {

                    Toast.makeText(RegisterActivity.this, "Please fill all details ", Toast.LENGTH_SHORT).show();

                }
                else {
                    Data workerData = new Data.Builder()
                            .putString(RegisterWorker.EMAIL, (Objects.requireNonNull(email)))
                            .putString(RegisterWorker.PASSWORD, (Objects.requireNonNull(password)))
                            .build();

                    WorkManager.getInstance().enqueue(new OneTimeWorkRequest
                            .Builder(RegisterWorker.class)
                            .setInputData(workerData).build());

                    activityRegisterBinding.etEmail.setText("");
                    activityRegisterBinding.etPassword.setText("");
                    displayProgressDialog();
                }

            }
        });

        activityRegisterBinding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    // This method will be called when a RegisterEvent is posteddf
    @Subscribe
    public void onEvent(RegisterEvent registerEvent){

        pDialog.dismiss();
        Looper.prepare();
        Toast.makeText(this, registerEvent.message +"  " + registerEvent.success, Toast.LENGTH_SHORT).show();

        if(registerEvent.success){
            Intent intent = new Intent(RegisterActivity.this,UserHomeActivty.class);
            saveUser("logged");
            startActivity(intent);
            finish();
        }

    }

    private void saveUser(String email) {
        SharedPreferences.Editor editor = Shared_pref.edit();
        editor.putString("user",email);
        editor.apply();
    }

}