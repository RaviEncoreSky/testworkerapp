package com.example.testappravi;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.testappravi.adapter.MovieListAdapter;
import com.example.testappravi.databinding.ActivityUserHomeActivtyBinding;
import com.example.testappravi.eventsHandlers.MovieListEvent;
import com.example.testappravi.responseBody.MovieResponse;
import com.example.testappravi.utils.AlertDialogHelper;
import com.example.testappravi.workers.MovieListWorker;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class UserHomeActivty extends BaseAppActivity {

    List<MovieResponse> moviesListResponseData;
    RecyclerView rvRecyclerView1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityUserHomeActivtyBinding activityUserHomeActivtyBinding = ActivityUserHomeActivtyBinding.inflate(getLayoutInflater());
        setContentView(activityUserHomeActivtyBinding.getRoot());

        SharedPreferences newPreference = getSharedPreferences("details", MODE_PRIVATE);

        activityUserHomeActivtyBinding.toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                AlertDialogHelper.showAlertDialog(UserHomeActivty.this,
                        "Logout Warning",
                        "User will Logout ! Continue ?",
                        new AlertDialogHelper.TwoOptionCallBack() {
                            @Override
                            public void onPositiveAction(Dialog dialog) {
                                SharedPreferences.Editor edit = newPreference.edit();
                                edit.clear();
                                edit.commit();
                                Intent in = new Intent(UserHomeActivty.this, LoginActivity.class);
                                dialog.cancel();
                                startActivity(in);
                                finish();

                            }

                            @Override
                            public void onNegativeAction(Dialog dialog) {
                                dialog.cancel();
                            }
                        });

                return false;
            }
        });

        WorkManager.getInstance().enqueue(new OneTimeWorkRequest
                .Builder(MovieListWorker.class)
                .build());

        rvRecyclerView1 = activityUserHomeActivtyBinding.rvRecyclerView;

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(UserHomeActivty.this);
        rvRecyclerView1.setLayoutManager(linearLayoutManager);

    }


    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(MovieListEvent recyclerEvent){
        Looper.prepare();

        moviesListResponseData = recyclerEvent.getMovieResponseList();
        System.out.println("---size " + moviesListResponseData.size());

        MovieListAdapter adapterMovies = new MovieListAdapter( moviesListResponseData,this);
        rvRecyclerView1.setAdapter(adapterMovies);

    }

}