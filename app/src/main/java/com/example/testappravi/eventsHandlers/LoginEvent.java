package com.example.testappravi.eventsHandlers;

public class LoginEvent extends BaseEvent {

    public LoginEvent(String message, Boolean success) {
        super(message, success);
    }
}
