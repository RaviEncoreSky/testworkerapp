package com.example.testappravi.eventsHandlers;

public abstract class BaseEvent {
    public final String message;
    public final Boolean success;

    public BaseEvent(String message, Boolean success) {
        this.message = message;
        this.success = success;
    }

}
