package com.example.testappravi.eventsHandlers;

import com.example.testappravi.responseBody.MovieResponse;

import java.util.List;

public class MovieListEvent {
    private final String message;
    private List<MovieResponse> movieResponseList;

    public MovieListEvent(String message, List<MovieResponse> movieResponseList) {
        this.message = message;
        this.movieResponseList = movieResponseList;
    }

    public String getMessage() {
        return message;
    }

    public List<MovieResponse> getMovieResponseList() {
        return movieResponseList;
    }
}
