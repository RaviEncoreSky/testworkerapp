package com.example.testappravi.responseBody;

public class RegisterResponse {

    private String id;
    private String token;
    private String error = null;

    public RegisterResponse(String id, String token, String error) {
        this.id = id;
        this.token = token;
        this.error = error;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "RegisterResponse{" +
                "id='" + id + '\'' +
                ", token='" + token + '\'' +
                ", error='" + error + '\'' +
                '}';
    }

}
